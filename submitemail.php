<?php

require_once "recaptchalib.php";

$captcharesponse = $_POST['g-recaptcha-response'];
$secret = "6LdYy84UAAAAAGyEccRDZZUSzDXuYzcNXCBy_Be9";
$response = null;
$reCaptcha = new ReCaptcha($secret);

$response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"],$_POST["g-recaptcha-response"]);

function validateEmail($email)
{
   if(preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email))
	  return true;
   else
	  return false;
}

if($response != null && $response->success) 
{
	$to = "info@embedtron.com"; // Replace with your email address
	$subject = "Message from ".ucwords($_POST['name']); // Enter the subject here.

	//Validating email addres
	$email = $_POST['email'];

	//Validates the required fields
	if((strlen($_POST['name']) < 1 ) || (strlen($email) < 1 ) || (strlen($_POST['message']) < 1 ) || validateEmail($email) == FALSE)
	{
		$emailerror .= true;
	} else 
	{
		$emailerror .= false;

		//Composing the email
		$email_message =
			"Name: " . ucwords($_POST['name']) . "\n" .
			"Email: " . $email . "\n" .
			"Subject: " . $subject . "\n" .
			"Message: " . "\n" . $_POST['message'] . "\n";
	
		//Sending the email
		mail($to, $subject, $email_message);
	}
	
	if($emailerror == true) 
	{
		echo '<span>Please fill all the fields.</span>';
	}
	else
	{
		echo "<span>Message has been sent. Thank you!</span>";
	}	

	
} else {
	echo "<h1 style='color:red;'>captcha error occurred submitting the CAPTCHA</h1>";
	return;
}
